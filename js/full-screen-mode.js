(function ($, Drupal, drupalSettings) {
	Drupal.behaviors.analog_behavior = {
		attach: function (context, settings) {
			$(once('full_screen_mode', 'body')).each(function (element) {
				(function ($) {
					"use strict";
					$(document).ready(function () {

						if (document.fullscreenEnabled || document.webkitFullscreenEnabled) {
							var css_url = drupalSettings.css_url;

							const toggleBtn = document.querySelector('.js-full-screen-mode-btn');

							const styleEl = document.createElement('link');
							styleEl.setAttribute('rel', 'stylesheet');
							styleEl.setAttribute('href', css_url);
							styleEl.addEventListener('load', function () {
								toggleBtn.hidden = false;
							});
							document.head.appendChild(styleEl);

							toggleBtn.addEventListener('click', function () {
								if (document.fullscreen) {
									document.exitFullscreen();
								} else if (document.webkitFullscreenElement) {
									document.webkitCancelFullScreen()
								} else if (document.documentElement.requestFullscreen) {
									document.documentElement.requestFullscreen();
								} else {
									document.documentElement.webkitRequestFullScreen();
								}
							});

							document.addEventListener('fullscreenchange', handleFullscreen);
							document.addEventListener('webkitfullscreenchange', handleFullscreen);

							function handleFullscreen() {
								if (document.fullscreen || document.webkitFullscreenElement) {
									toggleBtn.classList.add('on');
									toggleBtn.setAttribute('aria-label', 'Exit fullscreen mode');
								} else {
									toggleBtn.classList.remove('on');
									toggleBtn.setAttribute('aria-label', 'Enter fullscreen mode');
								}
							}
						}

						var dimmableElement = $(".full-screen-mode-btn");
						var isActive = true;
						var timeout;

						function dimElement() {
							if (isActive) {
								dimmableElement.addClass("dim");
								isActive = false;
							}
						}

						function resetElement() {
							if (!isActive) {
								dimmableElement.removeClass("dim");
								isActive = true;
							}
						}

						$(document).mousemove(function () {
							clearTimeout(timeout);
							resetElement();

							timeout = setTimeout(dimElement, 3000);
						});

						timeout = setTimeout(dimElement, 3000);
					});

				})(jQuery);
			});
		}
	}
}(jQuery, Drupal, drupalSettings));