<?php

namespace Drupal\full_screen_mode\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Provides the Full Screen Mode.
 *
 * @Block(
 *   id="full_screen_mode",
 *   admin_label = @Translation("Full Screen Mode"),
 * )
 */
class FullScreenModeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return parent::defaultConfiguration() + [
      'full_screen_mode' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['button'] = [
      '#type' => 'details',
      '#title' => $this->t('Button configuration'),
      '#open' => true,
      '#description' => $this->t('Configure the button property')
    ];

    $options = array(
      'top_right' => $this->t('Top right'),
      'top_left' => $this->t('Top left'),
      'bottom_right' => $this->t('Bottom right'),
      'bottom_left' => $this->t('Bottom left'),
    );

    $form['button']['position'] = array(
      '#type' => 'select',
      '#required' => true,
      '#options' => $options,
      '#title' => $this->t('Button position'),
      '#description' => $this->t('This property controls how the button is positioned within the page.'),
      '#default_value' => $config['position'] ?? 'top_right',
    );

    $form['button']['top'] = array(
      '#type' => 'number',
      '#title' => $this->t('Top'),
      '#description' => $this->t('This property controls how the button is positioned relative to top of the page.'),
      '#default_value' => $config['top'] ?? '10',
      '#states' => [
        'visible' => [
          ':input[name="settings[button][position]"]' =>  [
            ['value' => 'top_right'],
            ['value' => 'top_left'],
          ],
        ],
      ],
      '#min' => 0,
      '#max' => 1025,
    );

    $form['button']['bottom'] = array(
      '#type' => 'number',
      '#title' => $this->t('Bottom'),
      '#description' => $this->t('This property controls how the button is positioned relative to bottom of the page.'),
      '#default_value' => $config['bottom'] ?? '0',
      '#states' => [
        'visible' => [
          ':input[name="settings[button][position]"]' =>  [
            ['value' => 'bottom_right'],
            ['value' => 'bottom_left'],
          ],
        ],
      ],
      '#min' => 0,
      '#max' => 1025,
    );

    $form['button']['right'] = array(
      '#type' => 'number',
      '#title' => $this->t('Right'),
      '#description' => $this->t('This property controls how the button is positioned relative to right of the page.'),
      '#default_value' => $config['right'] ?? '10',
      '#states' => [
        'visible' => [
          ':input[name="settings[button][position]"]' =>  [
            ['value' => 'bottom_right'],
            ['value' => 'top_right'],
          ],
        ],
      ],
      '#min' => 0,
      '#max' => 1025,
    );

    $form['button']['left'] = array(
      '#type' => 'number',
      '#title' => $this->t('Left'),
      '#description' => $this->t('This property controls how the button is positioned relative to left of the page.'),
      '#default_value' => $config['left'] ?? '0',
      '#states' => [
        'visible' => [
          ':input[name="settings[button][position]"]' =>  [
            ['value' => 'bottom_left'],
            ['value' => 'top_left'],
          ],
        ],
      ],
      '#min' => 0,
      '#max' => 1025,
    );

    $form['button']['width'] = array(
      '#type' => 'number',
      '#title' => $this->t('Button width'),
      '#description' => $this->t('This property controls the button width.'),
      '#default_value' => $config['width'] ?? '50',
      '#min' => 0,
      '#max' => 280,
    );

    $form['button']['unit'] = [
      '#type' => 'select',
      '#title' => $this->t('Measurement unit'),
      '#options' => array(
        'px' => $this->t('px'),
        'em' => $this->t('em'),
        'rem' => $this->t('rem'),
        '%' => $this->t('%'),
      ),
      '#default_value' => $config['unit']??'px',
      '#description' => 'Unit of measurement for the button position relative to the page.'
    ];

    $form['appearance'] = [
      '#type' => 'details',
      '#title' => $this->t('Button appearance configuration'),
      '#open' => true,
      '#description' => $this->t('Configure button appearance on the page')
    ];

    $form['appearance']['stroke_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Stroke color'),
      '#default_value' => $config['stroke_color']??'#fff',
      '#required' => TRUE,
      '#description' => 'Button stroke color'
    ];

    $form['appearance']['stroke_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Stroke width'),
      '#default_value' => $config['stroke_width']??3,
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 50,
      '#description' => 'Button stroke width'
    ];

    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $config['active']??false,
      '#description' => 'Enable full screen mode'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $full_screen_mode_data_config = $this->configuration['full_screen_mode_data'] ?? '';
    $current_full_screen_mode_image_ids = array();
    $current_header_image_ids = array();
    $items = array();

    $values = $form_state->getValues();

    $position = $values['button']['position'] ?? 'top_right';
    $top = $values['button']['top'] ?? '';
    $right = $values['button']['right'] ?? '';
    $bottom = $values['button']['bottom'] ?? '';
    $left = $values['button']['left'] ?? '';
    $width = $values['button']['width'] ?? '50';
    $unit = $values['button']['unit'] ?? 'px';
    $stroke_width = $values['appearance']['stroke_width'] ?? '0';
    $stroke_color = $values['appearance']['stroke_color'] ?? '1';
    $active = $values['active'] ?? false;

    if($position == 'top_right') {
      $top = ($top != '')?$top:0;
      $right = ($right != '')?$right:0;

    } else if($position == 'top_left') {
      $top = ($top != '')?$top:0;
      $left = ($left != '')?$left:0;

    } else if($position == 'bottom_right') {
      $bottom = ($bottom != '')?$bottom:0;
      $right = ($right != '')?$right:0;

    } else {
      $bottom = ($bottom == '')?$bottom:0;
      $left = ($left == '')?$left:0;
    }

    $this->configuration['position'] = $position;
    $this->configuration['top'] = $top;
    $this->configuration['right'] = $right;
    $this->configuration['bottom'] = $bottom;
    $this->configuration['left'] = $left;
    $this->configuration['width'] = $width;
    $this->configuration['unit'] = $unit;
    $this->configuration['stroke_width'] = $stroke_width;
    $this->configuration['stroke_color'] = $stroke_color;
    $this->configuration['active'] = $active;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    $active = $config['active']??false;
    if($active) {
      $full_screen_mode_data = array();

      $position = $config['position']??'top_right';
      $top = $config['top']??'0';
      $right = $config['right']??'0';
      $bottom = $config['bottom']??'0';
      $left = $config['left']??'0';
      $width = $config['width']??'50';
      $unit = $config['unit']??'px';
      $stroke_width = $config['stroke_width']??'3';
      $stroke_color = $config['stroke_color']??'#000';

      if($position == 'top_right') {
        $buton_array['top'] = $top.$unit;
        $buton_array['right'] = $right.$unit;

      } else if($position == 'top_left') {
        $buton_array['top'] = $top.$unit;
        $buton_array['left'] = $left.$unit;

      } else if($position == 'bottom_right') {
        $buton_array['bottom'] = $bottom.$unit;
        $buton_array['right'] = $right.$unit;

      } else {
        $buton_array['bottom'] = $bottom.$unit;
        $buton_array['left'] = $left.$unit;

      }

      $build = [
        '#cache' => [
          'max-age' => 0,
        ],
      ];

      $path_css = " stroke: {$stroke_color};
      stroke-width: {$stroke_width}";

      $unit = ($unit != '%')?$unit:'px;';

      $svg_css = "width: {$width}{$unit};";

      $btn_class = $this->arrayToCss($buton_array);
      $full_screen_mode_data = array(
        'btn_class' => $btn_class,
        'path_css' => $path_css,
        'svg_css' => $svg_css,
      );

      $build['full_screen_mode'] = [
        '#theme' => 'full_screen_mode',
        '#full_screen_mode_data' => $full_screen_mode_data,
      ];

      $css_path = drupal_get_path('module', 'full_screen_mode') . '/css/full-screen-mode.css';

      $css_url = file_create_url($css_path);
      $build['#attached']['drupalSettings']['css_url'] =  $css_url;
      $build['#attached']['library'][] = 'full_screen_mode/full_screen_mode';

      return $build;
    }
  }

  // Function to convert an array to a CSS class string
  function arrayToCss($cssArray) {
    $cssRule = '';

    foreach ($cssArray as $property => $value) {
      $cssRule .= $property . ': ' . $value . ';';
    }

    return $cssRule;
  }

}